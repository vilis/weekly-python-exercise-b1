
def multiziperator(letters, numbers, symbols):
    for index in zip(letters, numbers, symbols):
        for element in index:
            yield element
                
                
letters = 'abcde'
numbers = [1,2,3,4,5,6,7,8,9]
symbols = '!@#$%'

for one_item in multiziperator(letters, numbers, symbols):
    print(one_item)

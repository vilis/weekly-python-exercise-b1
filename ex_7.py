import os

def file_length(filename):
    return os.stat(filename).st_size

def filefunc(dirname, func):
    success_dict = {}
    failure_dict = {}
        
    for filename in os.scandir(dirname):
        try:
            success_dict[filename.name] = func(dirname + filename.name)
        except Exception as e:
            failure_dict[filename.name] = e
        
    return success_dict, failure_dict

print(filefunc('/etc/', file_length))

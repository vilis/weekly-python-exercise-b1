import re


def line_slicing(line):
    ip = re.search(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})', line).group(0)
    time_stamp = re.search(r'\[(.*?)\]', line).group(0)
    request = re.search(r'GET(.*?)"', line).group(0)
    return {'ip_address': ip,
            'timestamp': time_stamp,
            'request': request}


def log_reader(path):
    "Converts log file into dictionary"

    with open(path, 'r') as reader:
        for line in reader:
            print(line_slicing(line))


if __name__ == "__main__":
    path = r""
    log_reader(path)

def myrange2(*args):
    l = []
    args = list(args)
    start = args[0]
    stop = args[1] if len(args) > 1 else start + 100
    step = args[2] if len(args) > 2 else 1
    while start < stop:
        l.append(start)
        start += step
    return l


def myrange3(*args):
    args = list(args)
    start = args[0]
    stop = args[1] if len(args) > 1 else start + 100
    step = args[2] if len(args) > 2 else 1

    while start < stop:
        yield start
        start += step


for x in myrange3(10, 30, 3):
    print(x, end=' ')

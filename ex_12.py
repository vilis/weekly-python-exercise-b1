import random


class RandomMemory:

    def __init__(self, min, max) -> None:
        self.min = min
        self.max = max
        self._history = []

    def history(self):
        return self._history
    
    @property
    def get(self):
        rand = random.randint(self.min, self.max+1)
        self._history.append(rand)
        return rand 
    
    
r = RandomMemory(1,100)
print(r.get)
print(r.get)
print(r._history)
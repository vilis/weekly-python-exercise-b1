from collections import namedtuple

Person = namedtuple("Person", "first last")


class TableFull(Exception):
    """Exception raised when the table is already full"""


class GuestList():
    def __init__(self) -> None:
        self.assigned = dict()
        self.unassigned_people = []

    def __len__(self):
        total_guests = len(self.unassigned_people)
        for guests in self.assigned.values():
            total_guests += len(guests)
        return total_guests

    def __str__(self):
        output = ""
        for table in sorted(self.assigned):
            output += f"{table}\n"
            for guest in self.assigned[table]:
                output += f"\t {guest.last} {guest.first}\n"
        return output

    def assign(self, person, table):
        for key, guests in self.assigned.items():
            if person in guests:
                if len(self.assigned[table]) == 10:
                    raise TableFull()
                else:
                    self.assigned[key].remove(person)
                    self.assigned[table].append(person)
                    return

        if table is None:
            self.unassigned_people.append(person)
            return

        if table not in self.assigned:
            self.assigned[table] = [person]
            return

        if len(self.assigned[table]) == 10:
            raise TableFull()
        else:
            self.assigned[table].append(person)

    def table(self, table_num):
        return [person
                for person in self.assigned[table_num]]

    def unassigned(self):
        return self.unassigned_people

    def free_space(self):
        spaces = {table: 10 - len(guests)
                  for table, guests in self.assigned.items()}
        return spaces


gl = GuestList()

gl.assign(Person('Waylon', 'Dalton'), 1)
gl.assign(Person('Justine', 'Henderson'), 1)
gl.assign(Person('Abdullah', 'Lang'), 3)
gl.assign(Person('Marcus', 'Cruz'), 1)
gl.assign(Person('Thalia', 'Cobb'), 2)
gl.assign(Person('Mathias', 'Little'), 2)
gl.assign(Person('Eddie', 'Randolph'), None)
gl.assign(Person('Angela', 'Walker'), 2)
gl.assign(Person('Lia', 'Shelton'), 3)
gl.assign(Person('Hadassah', 'Hartman'), None)
gl.assign(Person('Joanna', 'Shaffer'), 3)
gl.assign(Person('Jonathon', 'Sheppard'), 2)

# print(gl.assigned)
# print(gl.unassigned)
# print(gl.assigned)
# print(len(gl))
print(gl.table(1))
print(gl)


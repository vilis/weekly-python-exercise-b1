def average_age(people):
    avg_age = 0
    for count, element in enumerate(people):
        avg_age += element['age']

    return (avg_age)/(count+1)


def all_hobbies(people):
    hobbies = []
    for element in people:
        for hobby in element['hobbies']:
            hobbies.append(hobby)

    return set(hobbies)


def hobby_counter(people):
    hobbies = []
    for element in people:
        for hobby in element['hobbies']:
            hobbies.append(hobby)

    counter = dict()
    for element in hobbies:
        counter[element] = hobbies.count(element)

    return counter


def n_most_common(people, value):
    hobbies = hobby_counter(people)

    if 0 > value > len(hobbies):
        return "Invalid value"

    most_common = []
    for counter, hobby in enumerate(sorted(hobbies, key=hobbies.get, reverse=True)):
        if counter <= value - 1:
            most_common.append(hobby)
        else:
            return most_common

    return most_common


all_people = [{'name': 'Reuven', 'age': 50, 'hobbies': ['Python', 'cooking', 'reading']},
              {'name': 'Atara', 'age': 20, 'hobbies': [
                  'horses', 'cooking', 'art', 'reading']},
              {'name': 'Shikma', 'age': 18, 'hobbies': [
                  'Python', 'piano', 'cooking', 'reading']},
              {'name': 'Amotz', 'age': 15, 'hobbies': ['boxing', 'cooking']}]

print(average_age(all_people))
# print(all_hobbies(all_people))
# print(hobby_counter(all_people))
# print(n_most_common(all_people, 3))

import sys


class Tee:
    def __init__(self, *args):
        self.files = args

    def __exit__(self, *args) -> None:
        for file in self.files:
            file.close()

    def write(self, text):
        print(text)

        for file in self.files:
            try:
                file.write(text)
            except Exception as e:
                print(e)

    def writelines(self, text):
        print(text)

        for line in text:
            try:
                self.writelines(line)
            except Exception as e:
                print(e)


f1 = open(r'/Users/vilis/Documents/repos/Python_projects/weekly-python-exercise-b1/tee1.txt', 'w')
f2 = open(r'/Users/vilis/Documents/repos/Python_projects/weekly-python-exercise-b1/tee2.txt', 'w')
t = Tee(f1, f2)

t.write('abc\n')
t.write('def\n')
t.write('ghi\n')

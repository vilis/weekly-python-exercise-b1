import re

from datetime import datetime


class LogDicts:
    def __init__(self, pth):
        self.dict_container = []
        self.log_reader(pth)

    def dicts(self, key=None):
        if key is None:
            return self.dict_container
        else:
            return sorted(self.dict_container, key=key)

    def iterdicts(self, key=None):
        for log in self.dict_container:
            if key is None:
                yield log
            else:
                yield sorted(log, key=key)

    def for_ip(self, ip_addr, key=None):
        """Returns all records for a particular IP address"""
        if key is None:
            return [log for log in self.dict_container if log['ip_address'] == ip_addr]
        else:
            return sorted([log for log in self.dict_container if log['ip_address'] == ip_addr], key=key)

    def for_request(self, text: str, key=None):
        """Returns all records whose request contains text"""
        if key is None:
            return [log for log in self.dict_container if text in log['request']]
        else:
            return sorted([log for log in self.dict_container if text in log['request']])

    def by_ip_address(self, one_log_dict):
        return one_log_dict['ip_address']

    def by_ip_request(self, one_log_dict):
        return one_log_dict['request']

    def by_ip_timestamp(self, one_log_dict):
        return one_log_dict['timestamp']

    def time_extraction(self):
        time_container = []
        for log in self.dict_container:
            time_container.append((log,
                                   datetime.strptime(
                                       log['timestamp'].strip('[]'), "%d/%b/%Y:%H:%M:%S %z")))
        return time_container

    def earliest(self):
        return min(self.time_extraction(), key=lambda t: t[1])[0]

    def latest(self):
        return max(self.time_extraction(), key=lambda t: t[1])[0]

    def line_slicing(self, line):
        ip = re.search(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})', line).group(0)
        time_stamp = re.search(r'\[(.*?)\]', line).group(0)
        request = re.search(r'GET(.*?)"', line).group(0)
        return {'ip_address': ip,
                'timestamp': time_stamp,
                'request': request}

    def log_reader(self, path):
        "Converts log file into dictionary"

        with open(path, 'r') as reader:
            for line in reader:
                self.dict_container.append(self.line_slicing(line))


if __name__ == "__main__":
    ld = LogDicts(
        r"/Users/vilis/Documents/repos/Python_projects/weekly-python-exercise-b1/additional_files/mini-access-log.txt")

    # ld.dicts(key=ld.by_ip_address)
    print(ld.latest())
import hashlib
import os


class DirFileHash(dict):
    def __init__(self, path) -> None:
        super().__init__()
        for directory in os.listdir(path):
            self[directory] = hashlib.md5(str(directory).encode('utf-8')).hexdigest()
    
    def __getitem__(self, path):
        return super().__getitem__(path)
            
    
if __name__ == "__main__":
    d = DirFileHash("/etc/")
    print(d['passwd'])
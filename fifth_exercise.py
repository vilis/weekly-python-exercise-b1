import json
import pip._vendor.requests as requests
import csv


def cities_to_csv(url, filename):

    response = requests.get(url)
    json_data = json.loads(response.text)

    key_list = ['city', 'state', 'rank', 'population']
    json_data = [{k: d[k] for k in key_list} for d in json_data]

    data_file = open(f'{filename}', 'w')
    csv_writer = csv.writer(data_file, delimiter='\t')

    # csv_writer.writerow(json_data[0].keys()) # write the header

    for row in json_data:  # write data rows
        csv_writer.writerow(row.values())

    data_file.close()


gist_url = 'https://gist.githubusercontent.com/reuven/77edbb0292901f35019f17edb9794358/raw/2bf258763cdddd704f8ffd3ea9a3e81d25e2c6f6/cities.json?__s=bj5od61pskgsbe270nei'

cities_to_csv(gist_url, 'new_file.csv')

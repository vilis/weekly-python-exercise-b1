visits = {}


def collect_places():
    visits_formatted = {}
    for country, cities in visits.items():
        city_list = cities.split(',')
        unique_cities = set(city.strip() for city in city_list)
        formatted_cities = [f'{city} ({city_list.count(city)})' if city_list.count(
            city) > 1 else city for city in unique_cities]
        visits_formatted[country] = formatted_cities
    return visits_formatted


def display_places():
    if not visits:
        print('You have not visited any places.')
    else:
        print('Places you have visited:')
        format = '\n\t'
        for country, cities in visits.items():
            print(f'{country}{format}{format.join(cities)}')


while True:
    message = input('Enter a city and country (e.g. city, country): ')
    if not message:
        break
    if message.count(',') != 1:
        print('Invalid data')
        continue
    city, country = map(str.strip, message.split(','))
    if country in visits:
        visits[country] += f',{city}'
    else:
        visits[country] = city

visits_formatted = collect_places()
visits = visits_formatted.copy()
display_places()
